package me.juan.learning;

import me.juan.learning.calculator.CasioCalculator;
import me.juan.learning.calculator.ICalculator;
import org.junit.Assert;
import org.junit.Test;

import java.util.Random;

public class CalculatorTest {
    private static final ICalculator calculator = new CasioCalculator("Andres");
    private static final Random random = new Random();


    @Test
    public void testAdd() {
        double a = random.nextInt(100), b = random.nextInt(100);

        double expected = a + b;
        double result = calculator.add(a, b);

        Assert.assertEquals(expected, result, 0);
    }

    @Test
    public void testSubtract() {
        double a = random.nextInt(100), b = random.nextInt(100);

        double expected = a - b;
        double result = calculator.subtract(a, b);

        Assert.assertEquals(expected, result, 0);
    }

    @Test
    public void testMultiply() {
        double a = random.nextInt(100), b = random.nextInt(100);

        double expected = a * b;
        double result = calculator.multiply(a, b);

        Assert.assertEquals(expected, result, 0);
    }

    @Test
    public void testDivide() {
        double a = random.nextInt(100), b = random.nextInt(100);

        double expected = a / b;
        double result = calculator.divide(a, b);

        Assert.assertEquals(expected, result, 0);
    }

    @Test
    public void testPower() {
        double a = random.nextInt(100), b = random.nextInt(100);

        double expected = Math.pow(a, b);
        double result = calculator.power(a, b);

        Assert.assertEquals(expected, result, 0);
    }

}
