package me.juan.learning.historical;

import lombok.AllArgsConstructor;
import lombok.Data;
import me.juan.learning.calculator.ICalculator;
import me.juan.learning.calculator.Operation;

import java.util.Date;

@Data
@AllArgsConstructor
public class Historical {

    private final double a;
    private final double b;
    private final double result;
    private final Operation operation;
    private final ICalculator calculator;

    private String username;
    private final Date date = new Date();

    public String getUsername() {
        return username == null ? "Anonymous" : username;
    }

    @Override
    public String toString() {
        return String.format("Calculator: %s, Date: %s, User: %s, Operation: %s %s %s = %s", calculator.getBrand(), date, this.getUsername(), a, operation.getSymbol(), b, result);
    }

}
