package me.juan.learning.historical;

import lombok.AllArgsConstructor;
import me.juan.learning.calculator.ICalculator;
import me.juan.learning.calculator.Operation;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public class HistoricalManager {
    private final List<Historical> history = new ArrayList<>();
    private final ICalculator calculator;
    private final String username;


    public double addHistory(double a, double b, double result, Operation operation) {
        history.add(new Historical(a, b, result, operation, calculator, username));
        return result;
    }

    public void printHistory() {
        history.forEach(System.out::println);
    }
}
