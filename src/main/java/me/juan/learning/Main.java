package me.juan.learning;

import me.juan.learning.calculator.CasioCalculator;
import me.juan.learning.calculator.GenericCalculator;
import me.juan.learning.calculator.ICalculator;

import java.util.Random;

public class Main {
    private static final Random random = new Random();

    public static void main(String[] args) {
        int a = random.nextInt(100) + 1;
        int b = random.nextInt(100) + 1;

        CasioCalculator calculator = new CasioCalculator("Juan");

        calculator.add(a, b);
        calculator.subtract(a, b);
        calculator.multiply(a, b);
        calculator.divide(a, b);
        calculator.power(a, b);

        calculator.getHistoricalManager().printHistory();
    }
}
