package me.juan.learning.calculator;

public interface ICalculator {
    double add(double a, double b);
    double subtract(double a, double b);
    double multiply(double a, double b);
    double divide(double a, double b);
    double power(double a, double b);

    String getBrand();
    String getModel();

}
